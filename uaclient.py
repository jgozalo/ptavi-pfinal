#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""UACLIENT"""

import random
import shlex
import socket
import subprocess
import sys
import time
from xml.dom import minidom
from secrets import randbits
import os
import simplertp


def loguear(evento):
    evento = evento.replace("\r\n", " ")
    if evento.find("INVITE") != -1 or evento.find("BYE") != -1 \
            or evento.find("ACK") != -1 or evento.find("REGISTER") != -1:
        destino = " Sent to "
    elif evento.find("Starting") != -1 or evento.find("Finishing") \
            != -1 or evento.find("format") != -1:
        destino = " "
    else:
        destino = " Received from "
    hora = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
    linea_log = hora + destino + evento + "\r\n"
    f_log.write(linea_log)


CONFIG = ""
METHOD = ""
OPTION = ""

try:
    """CONFIG es el fichero XML de configuracion"""
    CONFIG = sys.argv[1]
    """METHOD es un metodo SIP"""
    METHOD = sys.argv[2].upper()
    """OPTION: parametro opcional dependiente de METHOD"""
    OPTION = sys.argv[3]

    """Acceder al fichero XML"""
    mydoc = minidom.parse(CONFIG)
    regproxy = mydoc.getElementsByTagName('regproxy')
    ip_regproxy = regproxy[0].attributes['ip'].value
    port_regproxy = int(regproxy[0].attributes['puerto'].value)

    account = mydoc.getElementsByTagName('account')
    name_account = account[0].attributes['username'].value

    uaserver = mydoc.getElementsByTagName('uaserver')
    puerto_uaserver = uaserver[0].attributes['puerto'].value
    ip_uaserver = uaserver[0].attributes['ip'].value

    rtpaudio = mydoc.getElementsByTagName('rtpaudio')
    puerto_rtpaudio = rtpaudio[0].attributes['puerto'].value

    audio_file = mydoc.getElementsByTagName('audio')
    audio_file = audio_file[0].firstChild.data

    log = mydoc.getElementsByTagName('log')
    log = log[0].firstChild.data
    f_log = open(log, "a")


except (IndexError, ValueError):
    loguear("Failed attempt due to incorrect format")
    sys.exit("Usage: python3 uaclient.py config method option")

try:
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((ip_regproxy, port_regproxy))

        if METHOD == "REGISTER":
            LINE = METHOD + " sip:" + name_account + ":" + \
                   puerto_uaserver + " SIP/2.0\r\n" \
                   + "Expires: " + OPTION + "\r\n\r\n"
            loguear("Starting")

        elif METHOD == "INVITE":
            """esperamos un name_account al cual invitamos"""
            sdp = "v=0" + "\r\n" + \
                  "o=" + name_account + " " + ip_uaserver + "\r\n" + \
                  "s=misesion" + "\r\n" + \
                  "t=0" + "\r\n" + \
                  "m=audio " + puerto_rtpaudio + " RTP\r\n"
            LINE = METHOD + " sip:" + OPTION + " SIP/2.0\r\n" + \
                            "Content-Type: application/sdp\r\n" + \
                            "Content-Length: " + \
                            str(len(sdp)) + "\r\n\r\n" + sdp

        elif METHOD == "BYE":
            LINE = METHOD + " sip:" + OPTION + " SIP/2.0\r\n\r\n"
        else:
            LINE = METHOD + " sip:" + OPTION + " SIP/2.0\r\n\r\n"

        my_socket.send(bytes(LINE, 'utf-8'))
        print("Enviando:\r\n" + LINE)
        loguear(ip_regproxy + ":" + str(port_regproxy) + ": " + LINE)

        data = my_socket.recv(1024)
        linea_recibida = data.decode('utf-8')
        loguear(ip_regproxy + ":" + str(port_regproxy) + ": " + linea_recibida)
        print('Rerecibido\r\n' + linea_recibida)
        ANSWER1 = "SIP/2.0 100 Trying\r\n\r\n"
        ANSWER2 = "SIP/2.0 180 Ringing\r\n\r\n"
        ANSWER3 = "SIP/2.0 200 OK\r\n\r\n"
        ANSWERS = ANSWER1 + ANSWER2 + ANSWER3

        if linea_recibida == ANSWER3:
            loguear("Finishing")
        elif linea_recibida.find(ANSWERS) != -1:
            # ejecutar comando para VLC en Shell
            mydoc1 = minidom.parse(CONFIG)
            rtpaudio = mydoc1.getElementsByTagName('rtpaudio')
            puerto_rtpaudio = rtpaudio[0].attributes['puerto'].value
            command_line = 'cvlc rtp://' + ip_uaserver + ":" + puerto_rtpaudio
            os.system(command_line + ' &')
            """args = shlex.split(command_line)
            subprocess.call(args)"""

            i_sdp = linea_recibida[linea_recibida.find("o="):
                                   linea_recibida.find("s=")]
            i_sdp = i_sdp.replace("\r\n", "")
            ip_simple = i_sdp.split(" ")[1]
            p_sdp = linea_recibida[linea_recibida.find("m="):
                                   linea_recibida.find("RTP")]
            puerto_simple = int(p_sdp.split(" ")[1])
            LINE = "ACK" + " sip:" + OPTION + " SIP/2.0\r\n\r\n"
            print("Enviando: " + LINE)
            my_socket.send(bytes(LINE, 'utf-8'))
            loguear(ip_regproxy + ":" + str(port_regproxy) + ": " + LINE)
            """ Ahora enviaremos RTP"""
            ALEAT = random.randint(1, 11111)
            ALEAT1 = random.randint(1, 11111)
            ALEAT2 = random.randint(1, 11111)
            ALEAT3 = random.randint(1, 11111)
            ALEAT4 = random.randint(1, 11111)
            BIT = int(randbits(1))
            RTP_header = simplertp.RtpHeader()
            csrc = [ALEAT1, ALEAT2, ALEAT3, ALEAT4]
            RTP_header.set_header(marker=BIT, ssrc=ALEAT)
            RTP_header.setCSRC(csrc)
            audio = simplertp.RtpPayloadMp3(audio_file)
            simplertp.send_rtp_packet(RTP_header, audio,
                                      ip_simple, puerto_simple)
        print("Terminando socket...")
except (ConnectionRefusedError, ConnectionError):
    loguear("Error. No server listening at: " +
            ip_regproxy + " port " + str(port_regproxy))
    f_log.close()
    sys.exit("Error. No server listening at: " +
             ip_regproxy + " port " + str(port_regproxy))

f_log.close()
print("Fin.")
