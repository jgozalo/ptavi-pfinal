#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
from socket import *
import socketserver
import sys
from secrets import randbits
import simplertp
import random
from xml.dom import minidom
import shlex
import subprocess
from subprocess import check_output


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    socketserver.UDPServer.allow_reuse_address = True
    dicc_sdp = {}

    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)
        global audio_file, ip_simple, puerto_simple
        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            line = self.rfile.read()
            if not line:
                break
            DECODED_LINE = line.decode('utf-8')
            print("El cliente nos manda " + DECODED_LINE)
            info = DECODED_LINE.split(" ")
            metodo = info[0]
            if metodo == "INVITE":
                i_sdp = DECODED_LINE[DECODED_LINE.find("o="):
                                     DECODED_LINE.find("s=")]
                i_sdp = i_sdp.replace("\r\n", "")
                ip_simple = i_sdp.split(" ")[1]
                p_sdp = DECODED_LINE[DECODED_LINE.find("m="):
                                     DECODED_LINE.find("RTP")]
                puerto_simple = int(p_sdp.split(" ")[1])
                origen_sdp = DECODED_LINE[DECODED_LINE.find("o="):
                                          DECODED_LINE.find("s=")]
                cuenta_origen_sdp = origen_sdp.split(" ")[0].split("=")[1]
                ip_origen_sdp = origen_sdp.split(" ")[1]
                self.dicc_sdp = {"ip": ip_origen_sdp,
                                 "account": cuenta_origen_sdp}
                print(self.dicc_sdp["ip"] + " ----- " +
                      self.dicc_sdp["account"])
                mydoc = minidom.parse(CONFIG)
                account = mydoc.getElementsByTagName('account')
                name_account = account[0].attributes['username'].value
                rtpaudio = mydoc.getElementsByTagName('rtpaudio')
                puerto_rtpaudio = rtpaudio[0].attributes['puerto'].value
                audio_file = mydoc.getElementsByTagName('audio')
                audio_file = audio_file[0].firstChild.data

                sdp = "v=0" + "\r\n" + \
                      "o=" + name_account + " " + ip_uaserver + "\r\n" + \
                      "s=misesion" + "\r\n" + \
                      "t=0" + "\r\n" + \
                      "m=audio " + puerto_rtpaudio + " RTP\r\n"

                respuesta = "SIP/2.0 100 Trying\r\n\r\n" \
                            "SIP/2.0 180 Ringing\r\n\r\n" \
                            "SIP/2.0 200 OK\r\n\r\n" + sdp
                self.wfile.write(respuesta.encode('utf-8'))
                print("Enviando:\r\n" + respuesta)

                # ejecutar comando para VLC en Shell
                mydoc1 = minidom.parse(CONFIG)
                rtpaudio = mydoc1.getElementsByTagName('rtpaudio')
                puerto_rtpaudio = rtpaudio[0].attributes['puerto'].value
                command_line = 'cvlc rtp://' + \
                               ip_uaserver + ":" + puerto_rtpaudio
                os.system(command_line + ' &')
                """args = shlex.split(command_line)
                subprocess.call(args)"""

            if metodo == "BYE":
                respuesta = "SIP/2.0 200 OK\r\n\r\n"
                self.wfile.write(respuesta.encode('utf-8'))
                print("Enviando:\r\n" + respuesta)
            if metodo == "ACK":
                """ Ahora enviaremos RTP"""
                print(str(puerto_simple))
                ALEAT = random.randint(1, 11111)
                ALEAT1 = random.randint(1, 11111)
                ALEAT2 = random.randint(1, 11111)
                ALEAT3 = random.randint(1, 11111)
                ALEAT4 = random.randint(1, 11111)
                BIT = int(randbits(1))
                RTP_header = simplertp.RtpHeader()
                csrc = [ALEAT1, ALEAT2, ALEAT3, ALEAT4]
                RTP_header.set_header(marker=BIT, ssrc=ALEAT)
                RTP_header.setCSRC(csrc)
                audio = simplertp.RtpPayloadMp3(audio_file)
                simplertp.send_rtp_packet(RTP_header, audio,
                                          ip_simple, puerto_simple)


if __name__ == "__main__":

    try:
        CONFIG = sys.argv[1]
    except IndexError:
        sys.exit("Usage: python3 uaserver.py config")

    """Acceder al fichero XML"""
    mydoc = minidom.parse(CONFIG)
    uaserver = mydoc.getElementsByTagName('uaserver')
    ip_uaserver = uaserver[0].attributes['ip'].value
    port_uaserver = int(uaserver[0].attributes['puerto'].value)

    # Creamos servidor de eco y escuchamos
    serv = socketserver.UDPServer((ip_uaserver, port_uaserver), EchoHandler)

    try:
        print("Listening...")
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
