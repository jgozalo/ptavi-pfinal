import json
import socket
import socketserver
import sys
import time
from xml.dom import minidom


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """ SIPRegisterHandler server class """

    diccionario_registro = ""
    """
    Inicializa un diccionario de diccionarios que guarda
    la informacion de los clientes
    (direccion, ip y hora de expiracion)
    """

    def loguear(self, direction, evento):
        evento = evento.replace("\r\n", " ")
        if direction == 0:  # received
            destino = " Received from "
        elif direction == 1:  # sent
            destino = " Sent to "
        else:
            destino = " "
        hora = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
        linea_log = hora + destino + evento + "\r\n"
        f_log = open(log, "a")
        f_log.write(linea_log)
        if evento.find("Finishing") != -1:
            f_log.close()

    def register2json(self):
        database = mydoc.getElementsByTagName('database')
        path_database = database[0].attributes['path'].value
        """Genera un fichero JSON con los datos del diccionario"""
        with open(path_database, 'w') as file:
            json.dump(self.diccionario_registro, file, indent=4)

    def json2register(self):
        database = mydoc.getElementsByTagName('database')
        path_database = database[0].attributes['path'].value

        re = open(path_database, 'r')
        contenido = re.read()
        re.close()

        if contenido == '':
            wr = open(path_database, 'w')
            wr.write("{}")
            wr.close()

        try:
            with open(path_database, 'r') as file:
                self.diccionario_registro = json.load(file)
        except FileNotFoundError:
            pass

    def handle(self):
        """
        Manejador de la clase SIPRegisterHandler
        (metodo que maneja todas las solicitudes)
        """
        self.json2register()
        mensaje_recibido = ''
        for line in self.rfile:
            mensaje_recibido += line.decode('utf-8')
            """
            Asigna la decodificacion del mensaje del cliente
            (convierte los Bytes a String)
            """

        # client_address es una tupla con los datos IP y puerto
        ip_cliente = self.client_address[0]
        puerto_cliente = int(self.client_address[1])

        print("IP CLIENTE: " + str(ip_cliente) +
              "  /  PUERTO CLIENTE: " + str(puerto_cliente))
        print("Mensaje del cliente --> ", mensaje_recibido)

        info = mensaje_recibido.split(' ')
        """ Crea una lista de la division por espacios"""
        metodo = info[0]
        address = info[1]
        dic_cuenta = address.split(":")[1]
        self.loguear(0, ip_cliente + ":" +
                     str(puerto_cliente) + ": " + mensaje_recibido)

        for cuenta in self.diccionario_registro.copy():
            hora_actual = int(time.time())
            hora_exp = self.diccionario_registro[cuenta]['register_date'] + \
                self.diccionario_registro[cuenta]['expires']
            if hora_actual >= hora_exp:
                del self.diccionario_registro[cuenta]
        if metodo == "REGISTER":
            expire = info[3]
            expire_int = int(expire)
            epoch_time = int(time.time())
            """Tiempo en segundos desde 1970"""
            puerto_servidor = int(address.split(":")[2])

            if expire_int > 0:
                self.diccionario_registro[dic_cuenta] = \
                    {"ip": "127.0.0.1",
                     "port": puerto_servidor,
                     "register_date": epoch_time,
                     "expires": expire_int}
            elif expire_int == 0:
                try:
                    del self.diccionario_registro[dic_cuenta]
                except KeyError:
                    pass
                print("El cliente " + dic_cuenta + " ha sido eliminado")

            """if self.diccionario_registro:
                print(self.diccionario_registro)"""

            self.register2json()
            ok_resp = 'SIP/2.0 200 OK\r\n'
            self.wfile.write(ok_resp.encode('utf-8'))
            self.loguear(1, str(ip_cliente) + ": " +
                         str(puerto_cliente) + ":" + ok_resp)
            """ Envia el mensaje de confirmacion al cliente"""
        elif metodo == "INVITE" or metodo == "ACK" or metodo == "BYE":
            try:
                with socket.socket(socket.AF_INET,
                                   socket.SOCK_DGRAM) as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    ip_sock = self.diccionario_registro[dic_cuenta]['ip']
                    port_sock = self.diccionario_registro[dic_cuenta]['port']
                    my_socket.connect((ip_sock, port_sock))
                    """
                    Se añaden las cabeceras cuando se envia hacia el servidor
                    (saber camino de vuelta)
                    #pero se quitan (no se añaden) cuando se retornan
                    campoVia = "Via: SIP 2.0/UDP " +
                    str(my_socket.getsockname()) +
                    "header added by proxy-registrar\r\n\r\n"
                    mensaje_recibido = mensaje_recibido[0:
                    mensaje_recibido.find("v=")-1]
                    + campoVia + mensaje_recibido[mensaje_recibido.find("v="):]
                    """
                    print("Enviando: " + mensaje_recibido)
                    my_socket.send(bytes(mensaje_recibido, 'utf-8'))
                    self.loguear(1, ip_sock + ":" +
                                 str(port_sock) + ": " +
                                 mensaje_recibido)
                    data = my_socket.recv(1024)
                    if data:
                        self.loguear(0, ip_sock + ":" +
                                     str(port_sock) + ": " +
                                     data.decode('utf-8'))

                    print(data.decode('utf-8'))
                    self.wfile.write(data)
                    if data:
                        self.loguear(1, ip_cliente + ":" +
                                     str(puerto_cliente) + ": " +
                                     data.decode('utf-8'))
                        if len(data.decode('utf-8')) < 23:
                            self.loguear(3, "Finishing")
            except KeyError:
                res404unf = 'SIP/2.0 404 User Not Found\r\n'
                self.wfile.write(res404unf.encode('utf-8'))
                self.loguear(1, ip_cliente + ":" +
                             str(puerto_cliente) + ": " + res404unf)

        else:
            res404mna = 'SIP/2.0 404 Method Not Allowed\r\n'
            self.wfile.write(res404mna.encode('utf-8'))
            self.loguear(1, ip_cliente + ":" +
                         str(puerto_cliente) + ": " + res404mna)


if __name__ == "__main__":
    try:
        CONFIG = sys.argv[1]

        mydoc = minidom.parse(CONFIG)
        server = mydoc.getElementsByTagName('server')
        ip_server = server[0].attributes['ip'].value
        if ip_server == "":
            ip_server = "127.0.0.1"
        port_server = server[0].attributes['puerto'].value
        name_server = server[0].attributes['name'].value
        log = mydoc.getElementsByTagName('log')
        log = log[0].firstChild.data

        hora = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
        f_log = open(log, "a")
        f_log.write(hora + " Starting\r\n")
        f_log.close()

        serv = socketserver.UDPServer((ip_server,
                                       int(port_server)),
                                      SIPRegisterHandler)

        print("Server " + name_server + " listening at port " + port_server)
        serv.serve_forever()

    except IndexError:
        print("Usage: python3 proxy_registrar.py config")
